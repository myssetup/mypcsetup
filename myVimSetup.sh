#!/bin/sh

# Clone Vundle
echo "Clone VundleVim for Vim Plugin Management"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Create vimrc
echo "set nocompatible" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "filetype off" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "set rtp+=~/.vim/bundle/Vundle.vim" >> ~/.vimrc
echo "call vundle#begin()" >> ~/.vimrc
echo "Plugin 'VundleVim/Vundle.vim'" >> ~/.vimrc
echo "Plugin 'altercation/vim-colors-solarized'" >> ~/.vimrc
echo "Plugin 'tomasr/molokai'" >> ~/.vimrc
echo "Plugin 'bling/vim-airline'" >> ~/.vimrc
echo "Plugin 'scrooloose/nerdtree'" >> ~/.vimrc
echo "Plugin 'jistr/vim-nerdtree-tabs'" >> ~/.vimrc
echo "Plugin 'airblade/vim-gitgutter'" >> ~/.vimrc
echo "call vundle#end()" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "filetype plugin indent on" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "set backspace=indent,eol,start" >> ~/.vimrc
echo "set ruler" >> ~/.vimrc
echo "set number" >> ~/.vimrc
echo "set showcmd" >> ~/.vimrc
echo "set incsearch" >> ~/.vimrc
echo "set hlsearch" >> ~/.vimrc
echo "set background=dark" >> ~/.vimrc
echo "set laststatus=2" >> ~/.vimrc
echo "syntax on" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "let g:airline_detect_paste=1" >> ~/.vimrc
echo "let g:airline#extensions#tabline#enabled=1" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "let NERDTreeShowHidden=1" >> ~/.vimrc
echo "map <C-n> :NERDTreeToggle<CR>" >> ~/.vimrc
echo "" >> ~/.vimrc
echo "highlight GitGutterAdd guifg=#009900 ctermfg=Green" >> ~/.vimrc
echo "highlight GitGutterChange guifg=#BBBB00 ctermfg=Yellow" >> ~/.vimrc
echo "highlight GitGutterDelete guifg=#ff2222 ctermfg=Red" >> ~/.vimrc
echo "map <C-l> :GitGutterLineHighlightsToggle<CR>" >> ~/.vimrc
echo "" >> ~/.vimrc

# Installing VIM Plugin
vim +PluginInstall
